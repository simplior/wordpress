<?php
/**
 * Plugin Name: Woocommerce Memberships AddOn
 * Description: Add product prices individually for each membership plans.
 * Version: 1.0
 * Author: Simplior
 * Author URI: https://www.simplior.com/
 * Text Domain: woocommerce-memberships-addons
 * Domain Path: /i18n/languages/
 * Requires at least: 5.6
 * Requires PHP: 7.0
 *
 * @package Woocommerce Memberships AddOn
 */

/**
 * Returns the main instance of WC.
 *
 * @since  1.0
 * @return Woocommerce Memberships AddOn
 */
function cws_plugin_init() { // phpcs:ignore WordPress.NamingConventions.ValidFunctionName.FunctionNameInvalid
	if ( ! class_exists( 'WC_Memberships_Loader' ) ) {
		return;
	} else {
		require plugin_dir_path( __FILE__ ) . 'includes/class-woo-membership-addons.php';
		$class_woo_membership_addons = new Woo_Membership_Addons();

		require plugin_dir_path( __FILE__ ) . 'includes/admin/class-woocommerce-membership-admin.php';
		$class_woo_membership_admin = new Woo_Membership_Admin();
	}
}
add_action( 'plugins_loaded', 'cws_plugin_init' );
