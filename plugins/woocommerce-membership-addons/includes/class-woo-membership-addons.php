<?php
/**
 * The Template for initializing plugin with action, filter hook.
 *
 * @package Woocommerce Memberships AddOn
 */

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'Woo_Membership_Addons' ) ) {

	/**
	 * Woo_Membership_Addons class.
	 */
	class Woo_Membership_Addons {

		/**
		 * Hook in constructor
		 */
		public function __construct() {

			add_filter( 'woocommerce_subscriptions_calculated_total', array( $this, 'wc_exclude_tax_cart_total' ), 10, 2 );

			add_filter( 'wc_memberships_members_area_my_membership_discounts_column_names', array( $this, 'cw_unset_discounts_column_names' ) );

			add_filter( 'wc_memberships_member_discount_badge', array( $this, 'cw_member_discount_badge' ) );
			add_filter( 'wc_memberships_variation_member_discount_badge', array( $this, 'cw_variation_member_discount_badge' ) );

			add_action( 'wp_head', array( $this, 'custom_header' ) );

			add_action( 'woocommerce_before_calculate_totals', array( $this, 'add_cart_custom_notice' ) );
		}

		/**
		 * Add order item via AJAX. This is refactored for better unit testing.
		 *
		 * @param string|array $cart_object Cart object.
		 */
		public function add_cart_custom_notice( $cart_object ) {
			wc_clear_notices();
			$options = get_option( 'veritiv_freight_setting' );
			if ( $options['enable'] ) {
				$apply_weight = $options['apply_weight'];
			} else {
				$apply_weight = $options['saia_apply_weight'];
			}

			$_product_weight_total = 0;
			$user_memberships = wc_memberships_is_user_member();
			if ( 1 === $user_memberships ) {
				foreach ( $cart_object->get_cart() as $item_values ) {
					$_product = $item_values['data'];
					if ( 'variation' === $_product->product_type ) {
						if ( wc_memberships_product_has_member_discount( $_product->variation_id ) ) {
							$_product_weight = ceil( $_product->get_weight() ) * $item_values['quantity'];
							$_product_weight_total = $_product_weight_total + $_product_weight;
						}
					} else {
						if ( wc_memberships_product_has_member_discount( $_product->id ) ) {
							$_product_weight = ceil( $_product->get_weight() ) * $item_values['quantity'];
							$_product_weight_total = $_product_weight_total + $_product_weight;
						}
					}
				}
				if ( $_product_weight_total < $apply_weight ) {
					global $woocommerce, $avada_woocommerce;
					remove_action( 'woocommerce_proceed_to_checkout', array( $avada_woocommerce, 'proceed_to_checkout' ), 10 );
					add_action( 'woocommerce_proceed_to_checkout', array( $this, 'disable_checkout_button_no_shipping' ), 11 );
					wc_add_notice( 'Product(s) in your cart does not meet the min ' . $apply_weight . 'lb limit per product. Please update the product(s) in question quantity', 'error' );
				}
			}
		}

		/**
		 * Disable checkout button with condition.
		 */
		public function disable_checkout_button_no_shipping() {
			echo '<a href="#" class="checkout-button button alt wc-forward">Proceed to checkout</a>';
		}

		/**
		 * Custom header filter
		 */
		public function custom_header() {
			$user_memberships = wc_memberships_is_user_member();
			if ( $user_memberships ) {
				?>
				<style type="text/css">
					.membership-menu-class{ display: none; }
				</style>
				<?php
			}
		}

		/**
		 * Add custom lable for pro discount
		 *
		 * @param string|array $badge Sale badge.
		 */
		public function cw_variation_member_discount_badge( $badge ) {
			$label = __( 'Pro Discount', 'woocommerce-memberships-addons' );
			$badge = '<span class="wc-memberships-variation-member-discount">' . esc_html( $label ) . '</span>';
			return $badge;
		}

		/**
		 * Add custom lable for pro discount
		 *
		 * @param string|array $badge Sale badge.
		 */
		public function cw_member_discount_badge( $badge ) {
			$label = __( 'Pro Discount', 'woocommerce-memberships-addons' );
			$badge = '<span class="onsale wc-memberships-member-discount">' . esc_html( $label ) . '</span>';
			return $badge;
		}

		/**
		 * Unset column in membership list
		 *
		 * @param string|array $columns Sale badge.
		 */
		public function cw_unset_discounts_column_names( $columns ) {
			unset( $columns['membership-discount-excerpt'] );
			return $columns;
		}

	} // end of class
} // end of class_exists
