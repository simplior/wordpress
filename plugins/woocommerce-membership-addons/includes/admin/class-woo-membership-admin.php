<?php
/**
 * Handles generic Admin functionailties
 *
 * @package Woocommerce Memberships AddOn
 */

defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'Woo_Membership_Admin' ) ) {

    /**
     * Woo_Membership_Admin class.
     */
    class Woo_Membership_Admin {

        /**
         * Hook in constructor
         */
        public function __construct()   {

            add_filter( 'manage_edit-wc_user_membership_columns',  array( $this, 'add_custom_member_columns' ),999 );
            add_action( 'manage_wc_user_membership_posts_custom_column', array( $this, 'custom_columns_contents' ), 10, 2 );
        }

        /**
         * Add order item via AJAX. This is refactored for better unit testing.
         *
         * @param string|array $column Get use membership columns.
         * @param string|array $post_id Get post id.
         */
        public function custom_columns_contents( $column, $post_id ) {
            $user_membership = wc_memberships_get_user_membership( $post_id );
            $user_id = $user_membership->get_user_id();
            switch ( $column ) {
                case 'm_document':
                    $document = get_user_meta( $user_id, '_membership_document', true );
                    $html = '';
                    $uploaddir = wp_upload_dir();
                    $dir_name = 'membership_addons';
                    $path = $uploaddir['baseurl'] . '/' . $dir_name . '/' . $user_id;
                    foreach ( $document as $key => $value ) {
                        $html .= '<a href="' . $path . '/' . $value . '" target="_blank">' . $value . '</a><br/>';
                    }
                    echo sprintf( '%s', $html );
                    break;
            }
        }

        /**
         * Add order item via AJAX. This is refactored for better unit testing.
         *
         * @param string|array $columns Get use membership columns.
         */
        public function add_custom_member_columns( $columns ) {
            $column_meta = array( 'm_document' => 'Documents' );
            $columns = array_slice( $columns, 0, 5, true ) + $column_meta + array_slice( $columns, 5, null, true );
            return $columns;
        }

    }
}
