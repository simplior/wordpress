/**
 * Main.js v1.0
 * Js for admin js
 */

jQuery(document).ready( function($) {
  jQuery('a.member_status').on('click', function () {
      jQuery('.form-wrapper').show();
      var email = $(this).data('email');
      var uid = $(this).data('uid');

      jQuery('.form-wrapper .user_data').attr({
          "data-email": email,
          "data-uid": uid
      });
  });
});