/**
 * Frontend.js v1.0
 * Js for frontend
 */

jQuery( document ).ready( function($) {
	$( '.country_select' ).on( 'change', function(e) {
        var fd = new FormData();
        fd.append( 'action', 'state_selection');
        fd.append( 'country_code', $(this).val() );
        $.ajax({
          url:cwajaxurl.ajaxurl,
          type:'POST',
          processData: false,
          contentType: false,
          data: fd,
          success: function( response ) {
            if( response != '' ){
              $('.state_select').replaceWith(response);
            }
          }
        });
    });
});
